#ifndef WRITER_HPP
#define WRITER_HPP

#include "MessageQueue.hpp"
#include <memory>
#include <string>
#include <thread>
#include <atomic>
#include <condition_variable>

using std::shared_ptr;
using std::string;
using std::atomic;
using std::condition_variable;
using osinittest::MessageQueue;

namespace osinittest {

class Writer : public IMessageQueueEvents {
 public:
  Writer(MessageQueue<string> *queue, int number, int sleep_ms);
    
  void run();

  void on_start();
  void on_stop();
  void on_hwm();
  void on_lwm();

 private:
  void log(const string& message);

  MessageQueue<string> *queue;  
  const int number;
  const int sleep_ms;
  atomic<bool> sleeping;
  atomic<bool> running;
  condition_variable cond_var_sleep;
};

}

#endif