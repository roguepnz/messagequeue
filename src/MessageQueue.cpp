#ifndef MESSAGE_QUEUE_CPP
#define MESSAGE_QUEUE_CPP

#include "MessageQueue.hpp"
#include <iostream>
#include <mutex>
#include <stdexcept>

using std::unique_lock;
using std::mutex;

namespace osinittest {

template <typename T>
MessageQueue<T>::MessageQueue(int queue_size, int lwm, int hwm) 
  : queue_size{queue_size}, lwm{lwm}, hwm{hwm} {
    
  if (lwm > hwm) {
    throw std::invalid_argument("lmw is greater than hwm");
  }

  if (hwm > queue_size) {
    throw std::invalid_argument("hwm is greater that queue_size");
  }
}

template <typename T>
RetCodes MessageQueue<T>::put(const T& item, int priority) {
  bool hwmed = false;
  {
    unique_lock<mutex> lock(queue_mutex);
    if (!is_running) {
      return RetCodes::STOPPED;
    }
    if (queue.size() >= queue_size) {
      return RetCodes::NO_SPACE;
    }
    queue.push(Node(item, priority));
    if (queue.size() >= hwm) {
      hwmed = true;
    }
  }
  empty_cond_var.notify_all();

  if (hwmed) {
    fire_hwmed();
    return RetCodes::HWM;
  }
  return RetCodes::OK;
}

template <typename T>
RetCodes MessageQueue<T>::get(T& item) {
  bool lwmed = false;
  {
    unique_lock<mutex> lock(queue_mutex);
    while (queue.empty() && is_running) {
      empty_cond_var.wait(lock);
    }
    if (!is_running) {
      return RetCodes::STOPPED;      
    }

    item = queue.top().item;
    queue.pop();
    if (queue.size() <= lwm) {
      lwmed = true;
    }
  }
  if (lwmed) {
    fire_lwmed();
  }
  return RetCodes::OK;
}

template <typename T>
void MessageQueue<T>::run() {
  {
    unique_lock<mutex> lock(queue_mutex);
    is_running = true;
  }
  fire_started();
}

template <typename T>
void MessageQueue<T>::stop() {
  {
    unique_lock<mutex> lock(queue_mutex);
    is_running = false;
  }
  empty_cond_var.notify_all();
  fire_stopped();
}

template <typename T>
void MessageQueue<T>::set_events(IMessageQueueEvents *listener) {
  listeners.push_back(listener);
}

template <typename T>
void MessageQueue<T>::fire_started() {
  for (const auto& listener: listeners) {
    listener->on_start();
  }
}

template <typename T>
void MessageQueue<T>::fire_stopped() {
  for (const auto& listener: listeners) {
    listener->on_stop();
  }
}

template <typename T>
void MessageQueue<T>::fire_hwmed() {
  for (const auto& listener: listeners) {
    listener->on_hwm();
  }
}

template <typename T>
void MessageQueue<T>::fire_lwmed() {
  for (const auto& listener: listeners) {
    listener->on_lwm();
  }
}

}

#endif
