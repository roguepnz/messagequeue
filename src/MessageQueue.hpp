#ifndef MESSAGE_QUEUE_HPP
#define MESSAGE_QUEUE_HPP

#include <memory>
#include <queue>
#include <vector>
#include <mutex>
#include <condition_variable>

using std::priority_queue;
using std::condition_variable;
using std::vector;
using std::shared_ptr;
using std::mutex;
using std::string;

namespace osinittest {

/**
 * Interface of MessageQueue listener.
 *
 */
class IMessageQueueEvents {
 public:
  virtual ~IMessageQueueEvents() { }
  
  virtual void on_start() = 0;
  virtual void on_stop() = 0;
  virtual void on_hwm() = 0;
  virtual void on_lwm() = 0;
};

enum class RetCodes : int {
  OK = 1,
  HWM = -1,
  NO_SPACE = -2,
  STOPPED = -3
};

template <typename T> class MessageQueue {
 public:
  MessageQueue(int queue_size, int lwm, int hwm);

  RetCodes put(const T& message, int priority);
  RetCodes get(T& message);

  void run();
  void stop();
  void set_events(IMessageQueueEvents *listener);

 private:
  void fire_started();
  void fire_stopped();
  void fire_hwmed();
  void fire_lwmed();

  class Node {
   public:
    Node (T item, int priority): item(item), priority(priority) { }
    T item;
    int priority;
    bool operator< (const Node& other) const {
      return priority < other.priority;
    }
  };

  priority_queue<Node> queue;
  vector<IMessageQueueEvents *> listeners;
  mutex queue_mutex;
  condition_variable empty_cond_var;
  bool is_running;
  const int queue_size;
  const int lwm;
  const int hwm;
};

}

#include "MessageQueue.cpp"

#endif