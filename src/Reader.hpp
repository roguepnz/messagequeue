#ifndef READER_HPP
#define READER_HPP

#include <string>
#include <memory>

#include "MessageQueue.hpp"

using std::string;

namespace osinittest {

class Reader {
 public:
  Reader(MessageQueue<string> *queue, int number, int sleep_ms);
  void run();

 protected:
  void handle_message(const string& message);

 private:
  void log(const string& message);

  MessageQueue<string> *queue;
  const int number;
  const int sleep_ms;
  bool is_running;
};

}

#endif