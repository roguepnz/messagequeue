#include "Writer.hpp"

#include <mutex>
#include <chrono>
#include <iostream>
#include <vector>
#include <string>

using std::mutex;
using std::cout;
using std::string;
using std::unique_lock;

namespace osinittest {

Writer::Writer(MessageQueue<string> *queue, int number, int sleep_ms) 
  : queue{queue}, number{number}, sleep_ms{sleep_ms}, sleeping{false}, running{true} { }

void Writer::run() {
  mutex m;
  unique_lock<mutex> lock(m);
  while(running.load()) {
    while (sleeping.load()) {
      cond_var_sleep.wait(lock);
    }

    string m = "message#" + std::to_string(number);
    int priority = number;
    auto code = queue->put(m, priority);
    if (code == RetCodes::OK) {
      log(m + ", " + std::to_string(priority));
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
  }
  log("stopped");
}
void Writer::on_start() { }

void Writer::on_stop() { 
  running.store(false);
  sleeping.store(false);
  cond_var_sleep.notify_all();
}

void Writer::on_hwm() {
  sleeping.store(true);
}

void Writer::on_lwm() {
  sleeping.store(false);
  cond_var_sleep.notify_all();
}

void Writer::log(const string& message) {
  cout << "[writer#" + std::to_string(number) + "]: " + message + "\n";
}

}