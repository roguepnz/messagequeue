#ifndef MAIN_HPP
#define MAIN_HPP

namespace osinittest {

class Main {
 public:
  Main(int queue_size, int lwm, int hwm, int num_readers, int num_writers);
  void main();

 private:
  const int queue_size;
  const int lwm;
  const int hwm;
  const int num_readers;
  const int num_writers;  
};

}

#endif