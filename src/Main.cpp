#include <string>
#include <iostream>
#include <queue>
#include <thread>
#include <chrono>
#include <functional>
#include <memory>

#include "Main.hpp"
#include "MessageQueue.hpp"
#include "Writer.hpp"
#include "Reader.hpp"

using osinittest::MessageQueue;
using osinittest::RetCodes;
using osinittest::Writer;
using osinittest::Reader;
using std::string;
using std::vector;
using std::thread;

namespace osinittest {

Main::Main(int queue_size, int lwm, int hwm, int num_readers, int num_writers)
  : queue_size{queue_size},
    lwm{lwm},
    hwm{hwm}, 
    num_readers{num_readers},
    num_writers{num_writers} { }

void Main::main() {
  MessageQueue<string> queue{queue_size, lwm, hwm};
  vector<Writer*> writers;
  vector<Reader*> readers;
  vector<thread> threads;

  for (auto i = 0; i < num_readers; i++) {
    readers.push_back(new Reader{&queue, i + 1, 100});
    threads.push_back(thread(std::bind(&Reader::run, readers[i])));
  }

  for (auto i = 0; i < num_writers; i++) {
    Writer w{&queue, i + 1, 100};
    writers.push_back(new Writer{&queue, i + 1, 100});
    queue.set_events(writers[i]);
    threads.push_back(thread(std::bind(&Writer::run, writers[i])));
  }

  std::cout << "START" << std::endl;
  queue.run();
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  std::cout << "STOP" << std::endl;
  queue.stop();

  for (auto& t : threads) {
    t.join();
  }
}

}
