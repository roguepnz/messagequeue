#include "Reader.hpp"
#include "MessageQueue.hpp"
#include <iostream>
#include <chrono>
#include <thread>
#include <string>

using std::string;
using std::cout;

namespace osinittest {

Reader::Reader(MessageQueue<string> *queue, int number, int sleep_ms) 
  : queue{queue}, number{number}, sleep_ms{sleep_ms}, is_running{true}  { }

void Reader::run() {
  string message;
  while(is_running) {
    std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
    auto code = queue->get(message);
    if (code == RetCodes::STOPPED) {
      is_running = false;
    } else {
      handle_message(message);
    }
  }
  log("stopped");
}

void Reader::handle_message(const std::string& message) {
  log(message);
}

void Reader::log(const string& message) {
  cout << "[reader#" + std::to_string(number) + "]: " + message + "\n";
}

}

